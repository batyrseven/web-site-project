//анологичен var_dump в php
function dump(obj) {
    var out = "";
    if (obj && typeof(obj) == "object") {
        for (var i in obj) {
            out += i + ": " + obj[i] + "<br>";
        }
    } else {
        out = obj;
    }
    //alert(out);
    $('#dump_view').html(out);
}

function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}
//START KUPON///////////////////////
function minus_kupon(id_cert_sub) {
    if ($('#count_kupon_' + id_cert_sub).text() > 0) {
        var kupon = parseInt($('#count_kupon_' + id_cert_sub).text()) - 1;
        $('#count_kupon_' + id_cert_sub).text(kupon);
        $('.count_kupon_' + id_cert_sub).val(kupon);
        var kupon_itogo_sum = parseInt($('#kupon_all_sum').text()) - parseInt($('#kupon_price_' + id_cert_sub).text());
        $('#kupon_all_sum').text(kupon_itogo_sum);
    }
    calc_summ_one_kupon(id_cert_sub);
}

function plus_kupon(id_cert_sub) {
    var kupon = parseInt($('#count_kupon_' + id_cert_sub).text()) + 1;
    $('#count_kupon_' + id_cert_sub).text(kupon);
    $('.count_kupon_' + id_cert_sub).val(kupon);
    var kupon_itogo_sum = parseInt($('#kupon_all_sum').text()) + parseInt($('#kupon_price_' + id_cert_sub).text());
    $('#kupon_all_sum').text(kupon_itogo_sum);
    calc_summ_one_kupon(id_cert_sub);
}

function calc_summ_one_kupon(id_cert_sub) {
    var all_sum_one_kupon = parseInt($('#count_kupon_' + id_cert_sub).text()) * parseInt($('#kupon_price_' + id_cert_sub).text());
    $('#all_sum_one_kupon_' + id_cert_sub).text(all_sum_one_kupon);
}

function search_user_card() {
    $.ajax({
        type: 'GET',
        url: '/partner/user/?card=' + $('#partner_card_num').val(),
        dataType: 'json',
        success: function(data) {
          if (data.meta.code == 200){
            data = data.data;
          }
          if (data.mobile) {
              $('#card_username').text(data.first_name + ' ' + data.last_name + '\n' + data.mobile).hide().fadeIn();
              if (data.avatar) {
                  $('#card_avatar').attr('src', data.avatar).hide().fadeIn();
              } else {
                  $('#card_avatar').attr('src', '/static/images/blank_avatar_220.png').hide().fadeIn();
              }
              $('#spisat_summu').css("background", "#48B14C");
              $('#spisat_summu').css("border-bottom-color", "#1C641C !important");
          } else {
              $('#card_username').text(data.message).hide().fadeIn();
              $('#card_avatar').attr('src', '').hide().fadeIn();
          }
        }
    });
}

function search_statistic_for_sms_code() {
    var data = {
        sms_code: $('#p_sms_code').val()
    }
    $.ajax({
        type: 'POST',
        url: 'request/search_statistic_for_sms_code.php',
        dataType: 'json',
        data: data,
        success: function(data) {
            if (data.user_name) {
                $('#p_card_username').text(data.user_name + '\n' + data.mphone).hide().fadeIn();
                $('#b_statistic_for_sms_code').removeAttr('disabled');
                $('#b_statistic_for_sms_code').css({
                    opacity: 1
                });
                $('#p_sms_code').attr('disabled', 'disabled');
                if (data.avatar) {
                    $('#p_card_avatar').attr('src', 'resources/images/users/190x190_' + data.avatar).hide().fadeIn();
                } else {
                    $('#p_card_avatar').attr('src', 'resources/images/blank_avatar_220.png').hide().fadeIn();
                }
            } else {
                $('#p_card_username').text(data.message).hide().fadeIn();
                $('#p_card_avatar').attr('src', '').hide().fadeIn();
            }
        }
    })
}
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
//покупаем предложения
function payment_offers(is_bezlimit) {
    $(function() {
        var offers = {};
        offers['certs'] = {};
        $('input[name="cert_amount"]').each(function() {
            //заносим в массив
            var cert_id = $(this).attr('cert-id');
            offers['certs'][cert_id] = parseInt($(this).val());
        });
        if ($('#kupon_all_sum').text() == 0) {
            alert('Выберите предложения!')
        } else {
            //dump(offers);
            offers['card_code'] = $('#partner_card_num').val();
            offers['card_pin'] = null;
            if ($('#partner_card_pin').val()) {
              offers['card_pin'] = $('#partner_card_pin').val();
            }
            offers['total'] = parseInt($('.proizvol_val').val());
            offers['is_bezlimit'] = is_bezlimit;
            $.ajax({
                type: 'POST',
                url: '/partner/buy',
                dataType: 'json',
                data: JSON.stringify(offers),
                contentType: "application/json",
                headers: {
                  'X-CSRFToken': getCookie('csrftoken')
                },
                error: function(res){
                  data = res.responseJSON;
                  $('.status_payment').html('<span style="color: red">' + (data && data.meta && data.meta.detail || 'Error') + '</span>').hide().fadeIn();
                },
                success: function(data) {
                    $('.status_payment').html('<span style="color: green">Успех</span>').hide().fadeIn();
                    // var timeout = setTimeout("location.reload(true);", 1000);

                    // function resetTimeout() {
                    //     clearTimeout(timeout);
                    //     timeout = setTimeout("location.reload(true);", 1000);
                    // }
                }
            })
        }
    })
}

function search_user_card_and_bezlimit() {
    var data = {
        card_code: $('#partner_card_num_bezlimit').val()
    }
    $.ajax({
        type: 'POST',
        url: 'request/partner_get_user_bezlimit.php',
        dataType: 'json',
        data: data,
        success: function(data) {
            if (data.user_name) {
                $('#card_username').text(data.user_name + '\n' + data.mphone).hide().fadeIn();
                if (data.avatar) {
                    $('#card_avatar').attr('src', 'resources/images/users/190x190_' + data.avatar).hide().fadeIn();
                } else {
                    $('#card_avatar').attr('src', 'resources/images/blank_avatar_220.png').hide().fadeIn();
                }
                // alert(data.statistic_bezlimit.date_end);
                if (data.statistic_bezlimit) {
                    $('#res2').text('до ' + data.statistic_bezlimit.date_end).hide().fadeIn();
                    if (data.statistic_bezlimit.status === 'Активен') {
                        $('#res1').text(data.statistic_bezlimit.status).hide().fadeIn().css("color", "#48B14C");
                    } else {
                        $('#res1').text(data.statistic_bezlimit.status).hide().fadeIn().css("color", "#ff0000");
                    }
                    $('#spisat_summu').css("background", "#48B14C");
                    $('#spisat_summu').css("border-bottom-color", "#1C641C !important");
                    $('#spisat_summu').attr('onclick', 'activation_one_day_bezlimit(' + data.statistic_bezlimit.id + ')');
                }
            } else {
                $('#card_username').text(data.message).hide().fadeIn();
                $('#card_avatar').attr('src', '').hide().fadeIn();
            }
        }
    })
}
//покупаем предложения
function activation_one_day_bezlimit(id_statistics_bezlimits) {
    $(function() {
        if (id_statistics_bezlimits == false) {
            alert('Покупка не найдена!')
        } else {
            //dump(offers);
            var offers = {};
            offers['card_code'] = $('#partner_card_num_bezlimit').val();
            offers['id_statistics_bezlimits'] = id_statistics_bezlimits;
            $.ajax({
                type: 'POST',
                url: 'request/activation_one_day_bezlimit.php',
                dataType: 'json',
                data: offers,
                success: function(data) {
                    if (data.code == 3) {
                        $('.status_payment').html('<span style="color: red">' + data.message + '</span>').hide().fadeIn();
                    } else if (data.code == 4) {
                        $('.status_payment').html('<span style="color: red">' + data.message + '</span>').hide().fadeIn();
                    } else if (data.code == 0) {
                        $('.status_payment').html('<span style="color: green">' + data.message + '</span>').hide().fadeIn();
                        var timeout = setTimeout("location.reload(true);", 1000);

                        function resetTimeout() {
                            clearTimeout(timeout);
                            timeout = setTimeout("location.reload(true);", 1000);
                        }
                    }
                }
            })
        }
    })
}
//покупаем предложения
function payment_offers_user() {
    $(function() {
        var offers = {};
        $('input[name^="offer"]').each(function() {
            //заносим в массив
            offers[$(this).attr('name')] = $(this).val();
        });
        if ($('#kupon_all_sum').text() == 0) {
            alert('Выберите предложения!')
        } else {
            //dump(offers);
            if (confirm("Вы действительно хотите купить предложения?")) {
                button_disabled('#spisat_summu', 'загрузка...');
                offers['card_code'] = $('#partner_card_num').val();
                offers['card_pin'] = $('#partner_card_pin').val();
                offers['total'] = $('.proizvol_val').val();
                $.ajax({
                    type: 'POST',
                    url: '?r=payment/offers_user',
                    dataType: 'json',
                    data: offers,
                    success: function(data) {
                        if (data.code == 3) {
                            $('.status_payment').html('<span style="color: red">' + data.message + '</span>').hide().fadeIn();
                            button_enabled('#spisat_summu', 'купить');
                        } else if (data.code == 4) {
                            $('.status_payment').html('<span style="color: red">' + data.message + '</span>').hide().fadeIn();
                            button_enabled('#spisat_summu', 'купить');
                        } else if (data.code == 0) {
                            $('.status_payment').html('<span style="color: green">' + data.message + '</span>').hide().fadeIn();
                           var timeout = setTimeout("location.reload(true);", 6000);

                           function resetTimeout() {
                               clearTimeout(timeout);
                               timeout = setTimeout("location.reload(true);", 6000);
                           }
                        }
                    }
                })
            }
        }
    })
}
//покупаем предложения
function payment_offers_user_no_card() {
    $(function() {
        var offers = {};
        $('input[name^="offer"]').each(function() {
            //заносим в массив
            offers[$(this).attr('name')] = $(this).val();
        });
        if ($('#kupon_all_sum').text() == 0) {
            alert('Выберите предложения!')
        } else {
            //dump(offers);
            if (confirm("Вы действительно хотите купить предложения?")) {
                button_disabled('#spisat_summu', 'загрузка...');
                offers['total'] = $('.proizvol_val').val();
                $.ajax({
                    type: 'POST',
                    url: 'request/payment_offers_user_no_card.php',
                    dataType: 'json',
                    data: offers,
                    success: function(data) {
                        if (data.code == 3) {
                            $('.status_payment').html('<span style="color: red">' + data.message + '</span>').hide().fadeIn();
                            button_enabled('#spisat_summu', 'купить');
                        } else if (data.code == 4) {
                            $('.status_payment').html('<span style="color: red">' + data.message + '</span>').hide().fadeIn();
                            button_enabled('#spisat_summu', 'купить');
                        } else if (data.code == 0) {
                            $('.status_payment').html('<span style="color: green">' + data.message + '</span>').hide().fadeIn();
                            var timeout = setTimeout("location.reload(true);", 6000);

                            function resetTimeout() {
                                clearTimeout(timeout);
                                timeout = setTimeout("location.reload(true);", 6000);
                            }
                        }
                    }
                })
            }
        }
    })
}
//покупаем произволькую сумму
function proizvol_sum_get() {
    if (!$('.proizvol_val').val()) {
        alert('Введите сумму');
        $('.proizvol_val').focus();
    } else {
        //dump(offers);
        var offers = {};
        offers['card_code'] = $('#partner_card_num').val();
        offers['card_pin'] = $('#partner_card_pin').val();
        offers['total'] = $('.proizvol_val').val();
        offers['sms_code'] = $('#podtverj_sms').val();
        $.ajax({
            type: 'POST',
            url: 'request/payment_offers.php',
            dataType: 'json',
            data: offers,
            success: function(data) {
                if (data.code == 3) {
                    alert(data.message);
                } else if (data.code == 4) {
                    alert(data.message);
                } else if (data.code == 2) {
                    alert(data.message);
                    $('#podtverj_sms').show();
                    $('#podtverj_sms').focus();
                    $("#spisat_summu").attr('disabled', true);
                    $("#spisat_summu").css({
                        'opacity': 0.39
                    });
                }
                //Сма подтверждение
                else if (data.code == 5) {
                    alert(data.message);
                    $('#podtverj_sms').show();
                    $('#podtverj_sms').focus();
                } else if (data.code == 0) {
                    alert(data.message);
                    location.reload();
                }
            }
        })
    }
}
//Делаем кнопку активной
$(function() {
    $("#podtverj_sms").keyup(function() {
        $("#spisat_summu").attr('disabled', false);
        $("#spisat_summu").css({
            'opacity': 1
        });
    });
    //ввод только цифр
    $('.proizvol_val, #p_sms_code').keypress(function(e) {
        if (e.which != 8 && e.which != 0 && e.which != 109 && e.which != 188 && e.which != 190 && (e.which < 48 || e.which > 57)) return false;
    });
    $("#partner_card_num").on('keyup', function() {
        //    alert('s');
        if ($("#partner_card_num").val().length == 8) {
            $('#partner_card_pin').focus();
            search_user_card()
        }
    });
    $("#partner_card_num_bezlimit").on('keyup', function() {
        //    alert('s');
        if ($("#partner_card_num_bezlimit").val().length == 8) {
            search_user_card_and_bezlimit();
        }
    });
    $("#p_sms_code").on('keyup', function() {
        if ($("#p_sms_code").val().length == 4) {
            search_statistic_for_sms_code();
        }
    });
});

function return_offer(pk) {
    ex1 = confirm("Вы уверены?");
    if (ex1) {
        var data = {
            "pk": pk
        }
        $.ajax({
            type: 'POST',
            url: '/partner/return',
            dataType: 'json',
            data: JSON.stringify(data),
            contentType: "application/json",
            headers: {
              'X-CSRFToken': getCookie('csrftoken')
            },
            error: function(res){
              data = res.responseJSON;
              alert(data && data.meta && data.meta.detail || 'Error');
            },
            success: function(data) {
                alert('Успех');
            }
        });
    }
}

function confirmation_offer(id_statistic, type) {
    // alert(id);
    //Выведим окно запроса и сохраним возвращенное значение в переменную ex1
    ex1 = prompt('Введите sms пароль для подтверждения:');
    //Отобразим имя введенное пользователем в окно запроса на страницу
    // document.write('Ваше имя: ' + ex1);
    if (ex1) {
        var data = {
            sms_code: ex1
        }
        $.ajax({
            type: 'POST',
            url: '/partner/activate-sms',
            dataType: 'json',
            data: JSON.stringify(data),
            contentType: "application/json",
            headers: {
              'X-CSRFToken': getCookie('csrftoken')
            },
            error: function(res){
              data = res.responseJSON;
              alert(data && data.meta && data.meta.detail || 'Error');
            },
            success: function(data) {
                alert('Успех');
            }
        });
    } else if (ex1 === '') {
        alert("sms Пароль неверный!");
    }
}

function confirmation_offer_one() {
    $(function() {
        var data = {
            sms_code: $('#p_sms_code').val()
        }
        if (data.sms_code == '') {
            alert('Введите смс код!')
        } else {
            //dump(offers);
            $.ajax({
                type: 'POST',
                url: '/partner/activate-sms',
                dataType: 'json',
                data: JSON.stringify(data),
                contentType: "application/json",
                headers: {
                  'X-CSRFToken': getCookie('csrftoken')
                },
                error: function(res){
                  data = res.responseJSON;
                  alert(data && data.meta && data.meta.detail || 'Error');
                },
                success: function(data) {
                    alert('Успех');
                }
            });
        }
    })
}

function kupit_button() {
    if ($('#kupon_all_sum').text() == 0) {
        alert('Выберите предложения!')
    } else {
        $("#total_certs_card").fadeIn();
        $("#cert_subs").hide();
    }
}

function exit_total() {
    $("#total_certs_card").hide();
    $("#cert_subs").fadeIn();
}

function user_bonus_add() {
    if (!$('.proizvol_val').val()) {
        alert('Введите сумму');
        $('.proizvol_val').focus();
    } else {
        //dump(offers);
        var offers = {};
        offers['card_code'] = $('#partner_card_num').val();
        offers['card_pin'] = $('#partner_card_pin').val();
        offers['total'] = $('.proizvol_val').val();
        offers['sms_code'] = $('#podtverj_sms').val();
        offers['chek'] = $('#n_chek').val();
        $.ajax({
            type: 'POST',
            url: 'request/partner_add_user_bonus.php',
            dataType: 'json',
            data: offers,
            success: function(data) {
                if (data.code == 3) {
                    alert(data.message);
                } else if (data.code == 4) {
                    alert(data.message);
                } else if (data.code == 2) {
                    alert(data.message);
                    $('#podtverj_sms').show();
                    $('#podtverj_sms').focus();
                    $("#spisat_summu").attr('disabled', true);
                    $("#spisat_summu").css({
                        'opacity': 0.39
                    });
                }
                //Сма подтверждение
                else if (data.code == 5) {
                    alert(data.message);
                    $('#podtverj_sms').show();
                    $('#podtverj_sms').focus();
                } else if (data.code == 0) {
                    alert(data.message);
                    location.reload();
                }
            }
        })
    }
}

function kupit_button2(sum, id_bezlimit_sub) {
    $('#kupon_all_sum').text(sum);
    $('#id_bezlimit_sub').val(id_bezlimit_sub);
    $("#total_certs_card").fadeIn();
    $("#cert_subs").hide();
}

function kupit_bezlimit_sub() {
    if (confirm("Вы действительно хотите купить предложения?")) {
        var data = {
            id_bezlimit_sub: $('#id_bezlimit_sub').val()
        }
        $.ajax({
            type: 'POST',
            url: 'request/add_statistics_bezlimits.php',
            data: data,
            success: function(data) {
                if (data == '1') {
                    alert("Покупка успешно подтверждена!");
                    location.reload();
                } else {
                    alert("По какимто причинам не удалось отправить запрос!");
                }
            }
        })
    }
}
//покупаем предложения
function payment_bezlimit_sub() {
    $(function() {
        if ($('#kupon_all_sum').text() == 0) {
            alert('Выберите предложения!')
        } else {
            //dump(offers);
            if (confirm("Вы действительно хотите купить предложения?")) {
                button_disabled('#spisat_summu', 'загрузка...');
                var offers = {};
                offers['id_bezlimit_sub'] = $('#id_bezlimit_sub').val();
                offers['card_code'] = $('#partner_card_num').val();
                offers['card_pin'] = $('#partner_card_pin').val();
                offers['total'] = $('.proizvol_val').val();
                $.ajax({
                    type: 'POST',
                    url: 'request/payment_bezlimit_user.php',
                    dataType: 'json',
                    data: offers,
                    success: function(data) {
                        if (data.code == 3) {
                            $('.status_payment').html('<span style="color: red">' + data.message + '</span>').hide().fadeIn();
                            button_enabled('#spisat_summu', 'купить');
                        } else if (data.code == 4) {
                            $('.status_payment').html('<span style="color: red">' + data.message + '</span>').hide().fadeIn();
                            button_enabled('#spisat_summu', 'купить');
                        } else if (data.code == 0) {
                            $('.status_payment').html('<span style="color: green">' + data.message + '</span>').hide().fadeIn();
                            var timeout = setTimeout("location.reload(true);", 6000);

                            function resetTimeout() {
                                clearTimeout(timeout);
                                timeout = setTimeout("location.reload(true);", 6000);
                            }
                        }
                    }
                })
            }
        }
    })
}

function search_user_bezlimits(search_url) {
    $.ajax({
        type: 'GET',
        url: search_url + $('#partner_card_num').val(),
        success: function(data){
            if(data){
              $('#user_purchases_list').html(data);
            }
        }
    });
}

function activate_bezlimit(pk) {
    var data = {
        "pk": pk
    };
    $.ajax({
        type: 'POST',
        url: '/partner/bezlimit/activate',
        data: JSON.stringify(data),
        dataType: 'json',
        data: JSON.stringify(data),
        contentType: "application/json",
        headers: {
          'X-CSRFToken': getCookie('csrftoken')
        },
        error: function(res){
            data = res.responseJSON;
            alert(data && data.meta && data.meta.detail || 'Error');
        },
        success: function(data) {
            alert('Успех');
        }
    });
}

jQuery(document).ready(function (e) {
    setMainContentBlockHeight(); // устанавливаем высоту центрального информационного блока
    SliderArrowsHightShow(); // показываем / скываем стрелки переключения слайдов при узком экране
    $(window).resize(function () { // при изменении ширины окна браузера
        setMainContentBlockHeight(); // устанавливаем высоту центрального информационного блока
        SliderArrowsHightShow();// показываем / скываем стрелки переключения слайдов при узком экране
    });

    $('.top_menu_element').click(function () { // переключение вкладок верхнего меню
        if ($(this).hasClass('active') == false) {
            $('.top_menu_element').removeClass('active');
            $(this).addClass('active');
        }
    });

    // *******************************

    //#region my mod

    function justifyPaymentCategoriesMenu(menuItem) {
        // -- растягиваем меню платежей "Мобильный / Коммуналка ..." на ширину блока
        var categoryMenuTabs = menuItem.find('.platezhi_logo_menu_tab_inner');
        var platezhi_logo_menu_tab_inner_divs_sum = 0;
        var platezhi_logo_menu_tab_inner_divs_sum_full = 0;
        $.each(categoryMenuTabs.find('div'), function () {
            var categoryTabItem = $(this);
            platezhi_logo_menu_tab_inner_divs_sum += parseInt(categoryTabItem.width());
            platezhi_logo_menu_tab_inner_divs_sum_full += parseInt(categoryTabItem.outerWidth());
        });
        if (platezhi_logo_menu_tab_inner_divs_sum_full > parseInt(categoryMenuTabs.parent().width())) {
            var val = Math.floor((parseInt($('.platezhi_logo_menu_tab_outer').width()) - platezhi_logo_menu_tab_inner_divs_sum - parseInt(categoryMenuTabs.find('div:first-child').css('padding-left'))) / (categoryMenuTabs.find('div').length - 1)) - 2;
            categoryMenuTabs.find('div').css('padding-right', Math.floor(val / 2));
            categoryMenuTabs.find('div:not(:first-child)').css('padding-left', Math.floor(val / 2));
        }
    }

    
    
    $('.main_inner').css('visibility', 'visible');
    $('.platezhi_logos_outer').css('visibility', 'visible');

    $('.change_block_change_pass').click(function () {
        window.location = 'index.aspx@action=settings';
    });

    $('.change_block_exit').click(function () {
        window.location = 'Logout.aspx';
    });
    
    // switch between accounts of the same type
    $('.account_select_tab').tabs({
        active: 0, activate: function () {
            main.contentHeight.resizeFrame();
        }
    });

    //#endregion
    
    // *******************************

    // события для работы со слайдером --
    //$('#slider_left_arrow').click(function () { // при клике на левую стрелку, переключем слайдер к следущему кадру
    //    SlideToRight(1);
    //});

    //$('#slider_right_arrow').click(function () { // при клике на правую стрелку, переключем слайдер к предыдущему кадру
    //    SlideToLeft(1);
    //});

    //$('.indicator').click(function () { // смены слайда через нижние индикаторы
    //    // num_active_slider_elem - порядковый номер отображаемого в данный момент слайда (счет с 1)
    //    if ($(this).index() + 1 == num_active_slider_elem) // если кликаем на слайд на котором мы находимся
    //        return false;
    //    else if ($(this).index() + 1 > num_active_slider_elem) // переключение к предыдущему слайду
    //        SlideToLeft(($(this).index() + 1) - num_active_slider_elem);
    //    else if ($(this).index() + 1 < num_active_slider_elem) // переключение к следующему слайду
    //        SlideToRight(num_active_slider_elem - ($(this).index() + 1));
    //});

    //if (getNumSliderElements() > 1) { // при загрузке страницы проверяем, если слайдов больше чем 1 показываем стрелку переключения к  следующему слайду
    //    if (parseInt($('.main_layout').width()) > 1130)
    //        $('#slider_right_arrow').show();
    //    $('.slider_indicators').show();
    //}
    // -- события для работы со слайдером

    // события для работы с баннерами --
    $('.column_right .li').mouseout(function () { // при потери элементом фокуса запускаем смену банеров с интервалом
        VisualsSlider('start');
    });

    $('.column_right .li').mouseover(function () { // при наведении курсора останавливаем смену банеров с интервалом и показываем баннер элемента на который навели		
        VisualsSlider('stop');
        $('#visuals_' + num_active_slider_elem + ' .li').removeClass('active');
        $('#visual_block_' + num_active_slider_elem).removeClass();
        $('#visual_block_' + num_active_slider_elem).addClass('visual_' + ($('#visuals_' + num_active_slider_elem + ' .li').index($(this)) + 1));
        $('#visual_block_' + num_active_slider_elem).addClass('visual_block');
        $('#visual_block_' + num_active_slider_elem).show();
        $(this).addClass('active');
    });

    $('.registration_button #Registration').click(function () {
        $(this).addClass('click');
    });

    $('#SignIn').click(function () {
        $(this).addClass('click');
    });

    $('.inputOuter input[type=text]').focus(function () {
        $($(this).parents('.inputOuter')).addClass('focus');
        $(this).removeClass('gray');
        if ($(this).val() == $(this).attr('rel') || $(this).val() == 0)
            $(this).val('');
    }).blur(function () {
        $($(this).parents('.inputOuter')).removeClass('focus');
        if ($(this).val() == '')
            $(this).val($(this).attr('rel'));
        if ($(this).val() == $(this).attr('rel'))
            $(this).addClass('gray');
    });

    // *******************************************
    //catch first click on platezhi-menu
    var flag = true;
    $('.platezhi_logo_menu_tab_inner div').click(function (e) { // переключаем пункты меню в выборе типа платежа --


        if ($(this).parent().hasClass('on_hover')) {

            //отключаем функцию наведения на меню
            $('.platezhi_logo_tabs_outer.on_hover .platezhi_logo_tabs_inner').unbind('mouseleave');
            $('.platezhi_logo_menu_tab_inner.on_hover').unbind('mouseleave');
            $('.platezhi_logo_menu_tab_inner div').unbind('mouseleave').unbind('mouseenter');

            //if (flag) {
            //    flag = false;
            //} else {
                if ($(this).hasClass('active')) {
                    $(this).removeClass('active');
                    pl_hide();
                } else {
                    pl_show(this);
                }
            //}
        }
        else {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                pl_hide();
            } else {
                pl_show(this);
            }
        }

        e.stopPropagation();

    }); // -- переключаем пункты меню в выборе типа платежа

    function pl_hide() {
        if ($("div").is(".opacity") == true) {
            $('.opacity').hide();
        };
        $('.platezhi_logo_tabs_inner').stop().animate(
            { 'height': 0 },
            200,
            function () {
                $('.platezhi_logo_tabs_inner div').hide();
            }
        );
    };

    function pl_show(i) {
        $('.platezhi_logo_menu_tab_inner div').removeClass('active');
        $(i).addClass('active');
        $('.platezhi_logo_tabs_inner div').hide();
        $('.platezhi_logo_tabs_inner #' + i.id).show();
        if ($("div").is(".opacity") == true) { // для страницы "Платежи", если пользователь не зашел в систему, показываем полупрозрачный bg --
            $('.opacity').show().css('height', $(window).height() - $('.opacity').offset().top);
        } // -- для страницы "Платежи", если пользователь не зашел в систему, показываем полупрозрачный bg
        $('.platezhi_logo_tabs_inner').stop().animate(
            {
                'height': $('div').is('.platezhi_logo_tabs_inner #' + i.id) == true
                           ? parseInt($('.platezhi_logo_tabs_inner #' + i.id).outerHeight()) + 'px'
                           : '0px'
            },
            200
        );
    };

    $('html').click(function () {
        // ловим клик вне блока
        if ($('.platezhi_logo_menu_tab_inner').hasClass('on_hover')) {
            $('.platezhi_logo_tabs_inner').stop().animate(
                { 'height': 0 },
                200,
                function () {
                    $('.platezhi_logo_tabs_inner div').hide();
                    pl_hide();
                    $('.platezhi_logo_menu_tab_inner div').removeClass('active');
                }
            );
        }
    });


    $('.hide_platezhi_logo_tab').click(function (e) { // скрываем меню при нажатии на стрелку --
        e.stopPropagation();
        $('.platezhi_logo_tabs_inner').css('height', parseInt($('.platezhi_logo_tabs_inner').height()) + 'px');
        $('.platezhi_logo_tabs_inner').addClass('platezhi_logo_tabs_inner_absolute');
        $('.platezhi_logo_menu_tab_inner').addClass('on_hover');
        $('.hide_platezhi_logo_tab').hide();
        $('.platezhi_logo_menu_tab_inner div').removeClass('active');
        if ($("div").is(".opacity") == true) {
            $('.opacity').hide();
        };
        $('.platezhi_logo_tabs_inner').stop().animate(
            { 'height': 0 },
            200,
            function () {
                $('.platezhi_logo_tabs_inner div').hide();
                $('.platezhi_logo_tabs_outer').addClass('on_hover');
                activateChangePaymentsOnHover();
                $.cookie("PAYMENT-PANEL-FLOATING", true, {
                    expires: 365
                });
            }
        );
    }); // -- скрываем меню при нажатии на стрелку

    if ($('.platezhi_logo_menu_tab_inner').size() && $('.platezhi_logo_menu_tab_inner').is('.on_hover')) { // на стартовой странице платежей сразу включаем переключение меню платежей по наведению --
    //    activateChangePaymentsOnHover();
    } // -- на стартовой странице платежей сразу включаем переключение меню платежей по наведению

    $('.platezhi_logo_tabs_inner div ins.logo_inner').click(function () { // событие click-а на иконку услуги в верхнем меню --
        $('.platezhi_logo_tabs_inner div ins.logo_inner').removeClass('active');
        $(this).addClass('active');
    }); // -- событие click-а на иконку услуги в верхнем меню


    // *******************************************

   

    VisualsSlider('restart'); // при загрузке страницы запускаем смену банеров с интервалом
});

var slider_elem_width = 960; // ширина блока со слайдером
var num_active_slider_elem = 1; // порядковый номер отображаемого в данный момент слайда (счет с 1)
var num_visual_elem = 0; // порядковый номер отображаемого в данный момент баннера (счет с 0)
var slider_processed = 0; // переменная происходит ли анимация переключения слайдера в данный момент, используется чтобы не запускать новую анимацию пока не отработала предыдущая
var slider_animation_index; // индекс анимации слайдера
var platezhi_logo_menu_tab_inner_hide_timeout;

// функции для работы со слайдером --
function getNumSliderElements() { // возвращает количество слайдов
    return $('#slider_blocks_ul li').length;
}

function changeSliderIndicator() { // смены слайда через нижние индикаторы
    $('.indicator').removeClass('active');
    $('.indicator:eq(' + (num_active_slider_elem - 1) + ')').addClass('active');
}

function SlideToLeft(step) { // переключение слайдера к следущему кадру
    if (Math.abs(parseInt($('#slider_blocks').css('left'))) + slider_elem_width * 2 <= getNumSliderElements() * slider_elem_width && slider_processed == 0) { // если текущий кадр не последний и в данный момент не идет анимация слайдера
        slider_processed = 1;
        $('#slider_right_arrow').addClass('active');
        $('#slider_blocks').animate({
            left: '-=' + slider_elem_width * step + 'px'
        }, 'fast', 'linear', function () {
            $('#slider_right_arrow').removeClass('active');
            $('#slider_left_arrow').show();
            slider_processed = 0;
            if (num_active_slider_elem + step <= getNumSliderElements()) {
                num_active_slider_elem += step;
                changeSliderIndicator();
            }
            if (num_active_slider_elem >= getNumSliderElements())
                $('#slider_right_arrow').hide();
            VisualsSlider('restart'); // запуск смены банеров с интервалом
        });
    }
}

function SlideToRight(step) { // переключение слайдера к предыдущему кадру
    if (parseInt($('#slider_blocks').css('left')) < 0 && slider_processed == 0) { // если текущий кадр не первый и в данный момент не идет анимация слайдера
        $('#slider_left_arrow').addClass('active');
        slider_processed = 1;
        $('#slider_blocks').animate({
            left: '+=' + slider_elem_width * step + 'px'
        }, 'fast', 'linear', function () {
            $('#slider_left_arrow').removeClass('active');
            $('#slider_right_arrow').show();
            slider_processed = 0;
            if (num_active_slider_elem - step >= 1) {
                num_active_slider_elem -= step;
                changeSliderIndicator();
            }
            if (num_active_slider_elem <= 1)
                $('#slider_left_arrow').hide();
            VisualsSlider('restart'); // запуск смены банеров с интервалом
        });
    }
}
// -- функции для работы со слайдером

function VisualsSlider(action) { // функция  для работы с баннерами. запуск смены банеров с интервалом
    if (action == 'restart') {
        num_visual_elem = 0;
        if (typeof (slider_animation_index) != 'undefined')
            clearInterval(slider_animation_index);
        action = 'start';
        $('#visuals_' + num_active_slider_elem + ' .li').removeClass('active');
        $('#visuals_' + num_active_slider_elem + ' .li:eq(0)').addClass('active');
        $('#visual_block_' + num_active_slider_elem).removeClass();
        $('#visual_block_' + num_active_slider_elem).addClass('visual_' + (num_visual_elem + 1));
        $('#visual_block_' + num_active_slider_elem).addClass('visual_block');
        $('#visual_block_' + num_active_slider_elem).show();
    }
    if (action == 'start') {
        slider_animation_index = setInterval(function () {
            $('#visuals_' + num_active_slider_elem + ' .li').removeClass('active');
            $('#visuals_' + num_active_slider_elem + ' .li:eq(' + num_visual_elem + ')').addClass('active');
            $('#visual_block_' + num_active_slider_elem).removeClass();
            $('#visual_block_' + num_active_slider_elem).addClass('visual_' + (num_visual_elem + 1));
            $('#visual_block_' + num_active_slider_elem).addClass('visual_block');
            $('#visual_block_' + num_active_slider_elem).show();
            if (num_visual_elem < $('#visuals_' + num_active_slider_elem + ' .li').length - 1)
                num_visual_elem++;
            else
                num_visual_elem = 0;
        }, 3000);
    } else if (action == 'stop')
        clearInterval(slider_animation_index);
}

function setMainContentBlockHeight() { // устанавливаем высоту центрального информационного блока
    //alert (parseInt(document.documentElement.scrollHeight));
    //$('.main_content').css('height', Math.max(parseInt(typeof(document.documentElement.scrollHeight) == 'undefined' ? document.body.clientHeight : document.documentElement.scrollHeight) - parseInt($('.main_top_block').outerHeight()) - parseInt($('.main_logo_call_block').outerHeight()) - parseInt($('.main_bottom_block').outerHeight()) - 1, 670) +'px');
    /*if (parseInt(document.documentElement.scrollHeight) <= parseInt(document.body.clientHeight) && ($("div").is(".main_bottom_block") == true))
		$('.main_layout').height(parseInt(document.documentElement.scrollHeight));
	*/
    //$('.main_content').css('height', Math.max(parseInt(typeof(document.documentElement.scrollHeight) == 'undefined' ? document.body.clientHeight : document.documentElement.scrollHeight) - parseInt($('.main_top_block').outerHeight()) - parseInt($('.main_logo_call_block').outerHeight()) - parseInt($('.main_bottom_block').outerHeight()) - 1, 670) +'px');
}

function SliderArrowsHightShow() { // показываем / скываем стрелки переключения слайдов при узком экране
    if (parseInt($('.main_layout').width()) <= 1130) {
        $('#slider_left_arrow').hide();
        $('#slider_right_arrow').hide();
    } else {
        if (num_active_slider_elem > 1)
            $('#slider_left_arrow').show();
        if (getNumSliderElements() > 1)
            $('#slider_right_arrow').show();
    }
}

function activateChangePaymentsOnHover() { // Функция активации смены платежа при наведении -- 
    var activeRegion = $('#tab_region div[aria-hidden="false"]');
    if (activeRegion.find('.platezhi_logo_menu_tab_inner').hasClass('on_hover')) {
        $('.platezhi_logo_tabs_inner').css('height', 0);
    }

    activeRegion.find('.platezhi_logo_menu_tab_inner.on_hover').mouseleave(function () {
        if (typeof (platezhi_logo_menu_tab_inner_hide_timeout) != 'undefined') {
            clearTimeout(platezhi_logo_menu_tab_inner_hide_timeout);
            platezhi_logo_menu_tab_inner_hide_timeout = undefined;
        }
        platezhi_logo_menu_tab_inner_hide_timeout = setTimeout(function () {
            activeRegion.find('.platezhi_logo_menu_tab_inner div').removeClass('active');
            //if ($("div").is(".opacity") == true) {
            if ($(".opacity").size()) {
                $('.opacity').hide();
            };
            activeRegion.find('.platezhi_logo_tabs_inner').stop().animate(
				{ 'height': 0 },
				200,
				function () {
				    activeRegion.find('.platezhi_logo_tabs_inner div').hide();
				}
			);
        }, 100);
    }); // -- переключаем пункты меню в выборе типа платежа

    activeRegion.find('.platezhi_logo_menu_tab_inner.on_hover div').mouseenter(function () { // переключаем пункты меню в выборе типа платежа --
        var this_o = $(this);

        platezhi_logo_menu_tab_inner_hide_timeout = setTimeout(function () {
            if (typeof (platezhi_logo_menu_tab_inner_hide_timeout) != 'undefined')
                clearTimeout(platezhi_logo_menu_tab_inner_hide_timeout);
            activeRegion.find('.platezhi_logo_menu_tab_inner div').removeClass('active');
            this_o.addClass('active');
            activeRegion.find('.platezhi_logo_tabs_inner div').hide();
            activeRegion.find('.platezhi_logo_tabs_inner #' + this_o.attr('id')).show();
            //if ($("div").is(".opacity") == true) {
            
            if ($(".opacity").length > 0) { // для страницы "Платежи", если пользователь не зашел в систему, показываем полупрозрачный bg --
                $('.opacity').show().css('height', $(window).height() - $('.opacity').offset().top);
            } // -- для страницы "Платежи", если пользователь не зашел в систему, показываем полупрозрачный bg

            activeRegion.find('.platezhi_logo_tabs_inner').stop().animate(
				{
				    //'height': $('div').is('.platezhi_logo_tabs_inner #' + this_o.attr('id')) == true
				    'height': $('.platezhi_logo_tabs_inner #' + this_o.attr('id')).size()
                               ? parseInt(activeRegion.find('.platezhi_logo_tabs_inner #' + this_o.attr('id')).outerHeight()) + 'px'
                               : '0px'
				},
				200
			);
        }, 100);
    });

    activeRegion.find('.platezhi_logo_tabs_outer.on_hover .platezhi_logo_tabs_inner').mouseenter(function () {
        if (typeof (platezhi_logo_menu_tab_inner_hide_timeout) != 'undefined') {
            clearTimeout(platezhi_logo_menu_tab_inner_hide_timeout);
            platezhi_logo_menu_tab_inner_hide_timeout = undefined;
        }
    }).mouseleave(function () {
        platezhi_logo_menu_tab_inner_hide_timeout = setTimeout(function () {
            activeRegion.find('.platezhi_logo_menu_tab_inner div').removeClass('active');
            //if ($("div").is(".opacity") == true) {
            if ($(".opacity").size()) {
                $('.opacity').hide();
            };
            activeRegion.find('.platezhi_logo_tabs_inner').stop().animate(
				{ 'height': 0 },
				200,
				function () {
				    activeRegion.find('.platezhi_logo_tabs_inner div').hide();
				}
			);
        }, 100);
    });
} // -- Функция активации смены платежа при наведении
function scroolImages(){
    $(document).ready(function(){
     
        $.mask.definitions['~']='[+-]';
        $('.item_photo').mousemove(function(e){
            images(this, e);
        });
        $('.item_photo').mouseenter(function(){
            if($(this).children('.item_photo_inner').attr('cnt') > 1){
                $(this).children('.item_photo_scrollbox').fadeIn();
            }
        })
        $('.item_photo').mouseleave(function(){
            if($(this).children('.item_photo_inner').attr('cnt') > 1){
                $(this).children('.item_photo_scrollbox').fadeOut();
            }
        })
        $('.item_list .item').mouseover(function(){
            $(this).css({
                'box-shadow': '0 1px 5px red'
            })
            $(this).find('.item_title').children('a').css({
                'border-bottom' : '1px solid #287d7d'
            });
        })
        $('.item_list .item').mouseleave(function(){
            $(this).css({
                'box-shadow': '0 1px 3px #1c344c'
            })
            $(this).find('.item_title').children('a').css({
                'border-bottom' : '1px solid #bed8d8'
            });
        });
        $('.tabs .tab').click(function(){
            tab_click(this);
        });

    })
}


function button_disabled(id, text) {
    $(id).attr("disabled", "disabled");
    $(id).css({
        opacity: 0.6
    });
    $(id).text(text);
}

function button_enabled(id, text) {
    $(id).removeAttr('disabled');
    $(id).css({
        opacity: 1
    });
    $(id).text(text);
}

function format(birth_date){
    birth_date.trim();
    return birth_date.substr(6).concat(birth_date.substr(2, 4), birth_date.substr(0, 2))
}

function send_email_frend(name, email, uid) {
    button_disabled('#' + uid, 'приглашение отправленно!');
    var data = {
        name: name,
        email: email
    };
    $.ajax({
        type: 'POST',
        url: 'request/send_email_frend.php',
        data: data,
        success: function(data) {
            if (data == 1) {} else {
                alert('По какимто причинам не удалось отправить запрос!');
            }
        }
    })
}
//START REGISTER///////////////////////
function register() {
    $(function() {
        if ($('#reg_firstname').val().length < 3) {
            alert('Введите ваше Имя!');
            $('#reg_firstname').focus();
        } else if (!validateEmail($('#reg_email').val())) {
            alert('Введите правельный E-mail!');
            $('#reg_email').focus();
        } else if ($('#reg_mphone').val().length < 11) {
            alert('Введите ваш сотовый телефон!');
            $('#reg_mphone').focus();
        } else {
            $('.RegistrationLogin_button_container').html('<img style="float: left; margin: 30px 0 0 150px" src="resources/images/loader_m.gif">');
            var data = {
                firstname: $('#reg_firstname').val(),
                lastname: $('#reg_lastname').val(),
                email: $('#reg_email').val(),
                mphone: $('#reg_mphone').val(),
                card_date: $('#reg_card_date').val(),
                card_num: $('#reg_card_num').val()
            };
            $.ajax({
                type: 'POST',
                url: '?r=site/reg',
                dataType: 'json',
                data: data,
                success: function(data) {
                    if (data.res == 1) {
                        alert('Введите сотовый телефон!');
                    } else if (data.res == 4) {
                        alert('Данный телефон уже зарегистрирован!');
                        $('#reg_mphone').focus();
                        $('.RegistrationLogin_button_container').html('<button style="width: 305px;" onClick="register()" type="submit" id="" name="" class="grey" tabindex="6">Зарегистрироваться</button>');
                    } else if (data.res == 3) {
                        alert('Успешная регистрация!');
						window.location.replace('?r=account/info');
                    } else {
                        alert('По каким то причинам регистрация не удалась!');
                    }
                }
            })
        }
    });
}
// $("#reg_card_num").keyup(function(event) {
//     if (event.keyCode == 13) {
//         register();
//     }
// });
//END REGISTER///////////////////////
//START LOGIN///////////////////////
function login() {
    if ($('#login_mphone').val().length < 17) {
        alert('Введите сотовый телефон!');
        $('#login_mphone').focus();
    } else if ($('#login_password').val().length < 4) {
        alert('Пароль должен содержать больше 3 символов!');
        $('#login_password').focus();
    } else {
        var data = {
            mphone: $('#login_mphone').val(),
            password: $('#login_password').val()
        };
        $.ajax({
            type: 'POST',
            url: '?r=site/auth',
            data: data,
            dataType: "json",
            success: function(data) {
                if (data) {
                    window.location.replace('?r=account/info');
                } else {
                    alert('Логин или пароль неправильный!');
                }
            }
        })
    }
}
$(function() {
        $("#login_password").keyup(function(event) {
            if (event.keyCode == 13) {
                login();
            }
        });
    })
    //END LOGIN/////////////////////////
    //CHANGE PASSWORD/////////////////////////
function change_password() {
    if ($('#password1').val().length < 5) {
        alert('Слишком короткий пароль!');
        $('#password1').focus();
    } else if ($('#password1').val() != $('#password2').val()) {
        alert('Введенные пароли не совпадают!');
        $('#password2').focus();
    } else {
        var data = {
            password1: $('#password1').val(),
            password2: $('#password2').val()
        };
        $.ajax({
            type: 'POST',
            url: '',
            data: data,
            dataType: "json",
            success: function(data) {
                if (data.res == 3) {
                    alert('Пароль успешно изменен!');
                    window.location = '/profile/';
                } else {
                    alert('По каким-то причинам пароль не изменен!');
                }
            }
        })
    }
}
//END CHANGE PASSWORD/////////////////////////
function change_pin_card() {
    if ($('#pin_card').val().length < 4) {
        alert('Пин должен быть четырехзначным!');
        $('#pin_card').focus();
    } else {
        var data = {
            pin_card: $('#pin_card').val()
        };
        $.ajax({
            type: 'POST',
            url: '?r = account/change_pin_card_action',
            data: data,
            dataType: "json",
            success: function(data) {
                if (data == 1) {
                    alert('Пин карты успешно изменен!');
                    location = "?r=account/info";
                } else {
                    alert('По каким-то причинам пароль не изменен!');
                }
            }
        })
    }
}

//ACCOUNT EDIT/////////////////////////
function account_edit(dest_url) {
    if ($('#e_firstname').val().length < 3) {
        alert('Введите ваше Имя!');
        $('#e_firstname').focus();
    } else if (!validateEmail($('#e_email').val())) {
        alert('Введите правельный E-mail!');
        $('#e_email').focus();
    } else if ($('#e_mphone').val().length < 10) {
        alert('Введите ваш сотовый телефон!');
        $('#e_mphone').focus();
    } else {
        var data = {
            first_name: $('#e_firstname').val(),
            last_name: $('#e_lastname').val(),
            email: $('#e_email').val(),
            mobile: $('#e_mphone').val(),
            birth_date: format($('#e_date').val()),
            sex: $('#e_sex option:selected').val(),
            city: $('#e_city option:selected').val()
        };
        $.ajax({
            type: 'POST',
            url: dest_url,
            data: data,
            success: function(data) {
                if (data == null || data === '0') {
                    alert("По каким то причинам данные не изменены!");
                }
                window.location = '/profile/';
            }
        })
    }
}

//END ACCOUNT EDIT/////////////////////////
function get_sms_password_to_user_phone() {
    if ($('#r_mphone').val().length < 11) {
        alert('Введите ваш сотовый телефон!');
        $('#r_mphone').focus();
    } else {
        var data = {
            mphone: $('#r_mphone').val()
        };
        $.ajax({
            type: 'POST',
            url: '?r=site/get_sms_password_to_user_phone',
            data: data,
            success: function(data) {
                if (data == '1') {
                    alert("На указанный телефон выслан смс с паролем!");
                    location = "?r=site/login";
                } else {
                    alert("Пользователь с указанным телефоном не найден!");
                }
            }
        })
    }
}

function searchCertsMainPage() {
    if ($('#searchCertsVal').val().length < 3) {
        $('#searchCertsVal').focus();
    } else {
        window.location = Kipa.search_url + '?q=' + $('#searchCertsVal').val();
    }
}
//START PARTNER LOGIN///////////////////////
function login_partner() {
    if ($('#partner_login').val().length < 2) {
        alert('Введите логин!');
        $('#partner_login').focus();
    } else if ($('#partner_pass').val().length < 4) {
        alert('Пароль должен содержать больше 3 символов!');
        $('#partner_pass').focus();
    } else {
        var data = {
            login: $('#partner_login').val(),
            pass: $('#partner_pass').val()
        };
        $.ajax({
            type: 'POST',
            url: 'request/partner_login.php',
            data: data,
            dataType: "json",
            success: function(data) {
                if (data.aut == 1) {
                    location = "partner_account";
                } else {
                    alert('Логин или пароль неправильный!');
                }
            }
        })
    }
}
$("#partner_pass").keyup(function(event) {
    if (event.keyCode == 13) {
        login_partner();
    }
});
//END PARTNER LOGIN/////////////////////////
// MENU
function set_menu() {
    $(document).ready(function() {
        $('#ddmenu li, #ddmenu_map li').hover(function() {
            clearTimeout($.data(this, 'timer'));
            $('ul', this).stop(true, true).slideDown(200);
        }, function() {
            $.data(this, 'timer', setTimeout($.proxy(function() {
                $('ul', this).stop(true, true).slideUp(200);
            }, this), 100));
        });
    });
}
set_menu();
//Action image scroll]
function scrollImage(obj, current, inner, lft, wdh) {
    if ($(inner).attr('current') != current) {
        scrl = $(obj).children('.item_photo_scrollbox').children('.item_photo_scroll');
        $(inner).queue("fx", []);
        $(scrl).queue("fx", []);
        $(inner).animate({
            left: '-' + (wdh * current) + 'px'
        }, 600);
        scrl.animate({
            left: lft + 'px'
        }, 600);
        $(inner).attr('current', current)
    }
}

function images(obj, e) {
    inner = $(obj).children('.item_photo_inner');
    cnt = inner.attr('cnt');
    if (cnt > 1) {
        posX = e.pageX - $(obj).offset().left;
        wdh = $(obj).width();
        lft_size = (wdh / cnt);
        if (cnt > 2) {
            rt_size = lft_size * 2;
        } else {
            rt_size = 9999;
        }
        if (posX > rt_size) {
            scrollImage(obj, 2, inner, rt_size, wdh);
        } else {
            if (posX > lft_size) {
                scrollImage(obj, 1, inner, lft_size, wdh);
            } else {
                scrollImage(obj, 0, inner, 0, wdh);
            }
        }
    }
}
$(document).ready(function() {
    //Кнопка Вверх
    $("#back-top").hide();
    $(function() {
        $(window).scroll(function() {
            if ($(this).scrollTop() > 50) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });
        $('#back-top a').click(function() {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });
    //search to intet
    $("#searchCertsVal").keyup(function(event) {
        if (event.keyCode == 13) {
            searchCertsMainPage();
        }
    });
    $.mask.definitions['~'] = '[+-]';
    $('.item_photo').mousemove(function(e) {
        images(this, e);
    });
    $('.item_photo').mouseenter(function() {
        if ($(this).children('.item_photo_inner').attr('cnt') > 1) {
            $(this).children('.item_photo_scrollbox').fadeIn();
        }
    })
    $('.item_photo').mouseleave(function() {
        if ($(this).children('.item_photo_inner').attr('cnt') > 1) {
            $(this).children('.item_photo_scrollbox').fadeOut();
        }
    })
    $('.item_list .item').mouseover(function() {
        $(this).css({
            'box-shadow': '0 1px 5px red'
        })
        $(this).find('.item_title').children('a').css({
            'border-bottom': '1px solid #287d7d'
        });
    })
    $('.item_list .item').mouseleave(function() {
        $(this).css({
            'box-shadow': '0 1px 3px #1c344c'
        })
        $(this).find('.item_title').children('a').css({
            'border-bottom': '1px solid #bed8d8'
        });
    });
    $('.tabs .tab').click(function() {
        tab_click(this);
    });
})

function get_comment_gift_completion(id_gift_completion) {
    if ($('#comment_gift_completion').val().length < 5) {
        alert('Введите комментарий!');
        $('#comment_gift_completion').focus();
    } else {
        var data = {
            id_gift_completion: id_gift_completion,
            comment: $('#comment_gift_completion').val()
        };
        $.ajax({
            type: 'POST',
            url: 'request/add_comment_gift_completion.php',
            data: data,
            success: function(data) {
                if (data == 1) {
                    alert('Ваш комментарий отправлен на модерацию!');
                    location.reload();
                    location = "gift_completion_view?id=" + id_gift_completion + "#tabs-3";
                } else {
                    alert('По каким то причинам не удалось отправить запрос!');
                }
            }
        })
    }
}
//Проверяем смс пароль и добавляем
function add_gift_completion_sms_users(id_gift_completion_sms) {
    if ($('#sms_code').val().length < 4) {
        alert('Введите полученный смс код!');
        $('#sms_code').focus();
    } else {
        var data = {
            id_gift_completion_sms: id_gift_completion_sms,
            sms_code: $('#sms_code').val()
        };
        $.ajax({
            type: 'POST',
            url: 'request/add_gift_completion_sms_users.php',
            data: data,
            success: function(data) {
                if (data == 1) {
                    alert('Поздравляем! Вы участвуете в розыгрыше');
                    location.reload();
                } else {
                    alert('Смс код не верный или же он уже был активирован!');
                }
            }
        })
    }
}

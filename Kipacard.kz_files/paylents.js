if (!payments) var payments = {};

payments.main = {
    init: function () {
        var p = this;
        $('.platezh .platezh_debt ins').click(function () {
            p.get_balance(this, true);
        }); // -- отрабатываем "узнать задолженность"
        $('.platezh .platezh_debt ins').each(function () {
            p.get_balance(this, false);
        });
    },
    hide_failed_payment_icon: function () {
        if ($('.payments_history_fail_status_ico').size()) {
            $('.payments_history_fail_status_ico').hide();
        }
    },
    show_failed_payment_icon: function () {
        if ($('#payments.active').size() && $('.payments_history_fail_status_ico').size()) {
            $('.payments_history_fail_status_ico').show();
        }
    },
    get_balance: function (inquiryLinkObj, clearCache) { // отрабатываем "узнать задолженность" --
        var debtInquiryLink = $(inquiryLinkObj);
        var inquiryType = debtInquiryLink.attr('inquiry_type');
        var subscriptionId;
        var serviceId;
        var parametersCollection;
        var requestData;
        var serviceUrl;

        if (inquiryType == 'subscription') {
            debtInquiryLink.hide();
            var obj = $('<img></img>').attr('src', 'resources/images/loader.gif');
            debtInquiryLink.parent().append($(obj));

            subscriptionId = debtInquiryLink.attr('subscription_id');
            requestData = "{subscriptionId:'" + subscriptionId + "', clearCache:'" + clearCache + "'}";
            serviceUrl = "PrivateServices/TelebankBackendService.asmx/GetBalance";
        }
        else if (inquiryType == 'raw') {
            debtInquiryLink.hide();
            var obj = $('<img></img>').attr('src', 'resources/images/loader.gif');
            debtInquiryLink.parent().append($(obj));
            serviceId = debtInquiryLink.attr('service_id');
            parametersCollection = $('#hdnServiceParameters').val().split('&');

            var paymentParameters = new Array();
            for (var i = 0; i < parametersCollection.length - 1; i++) {
                var values = parametersCollection[i].split('=');
                paymentParameters[values[0]] = $('#' + values[1]).val();
            }

            requestData = JSON.stringify({ serviceId: serviceId, paymentParameters: paymentParameters });
            serviceUrl = "PrivateServices/TelebankBackendService.asmx/GetBalanceEx";
        }

        function onBalanceRequestSuccess(msg) {
            if (msg.d != "") {
                if (msg.d == "logout") {
                    window.location.href = "Logout.aspx";
                }
                else {
                    $(obj).remove();

                    var balance = parseFloat(msg.d.replace(",", "."));

                    if (!isNaN(balance)) {
                        var now = new Date();
                        var outHour = now.getHours();
                        var outMin = now.getMinutes();

                        var hours = outHour > 9 ? outHour : "0" + outHour;
                        var mins = outMin < 10 ? "0" + outMin : outMin;

                        var timestamp = (hours + ':' + mins);

                        var messagePart1;
                        var messagePart2;

                        if (balance > 0) {
                            // переплата
                            messagePart1 = $('.positive_balance_part1').text();
                            messagePart2 = $('.positive_balance_part2').text();

                        } else {
                            // задалженность
                            messagePart1 = $('.negative_balance_part1').text();
                            messagePart2 = $('.negative_balance_part2').text();
                        }

                        if (messagePart1.indexOf('#TIMESTAMP#') != -1) {
                            messagePart1 = messagePart1.replace('#TIMESTAMP#', timestamp);
                        }

                        if (messagePart2.indexOf('#TIMESTAMP#') != -1) {
                            messagePart2 = messagePart2.replace('#TIMESTAMP#', timestamp);
                        }

                        debtInquiryLink.parent().find('.balance_type').text(messagePart1);
                        debtInquiryLink.parent().find('.balance_timestamp').text(messagePart2);

                        var balanceStr = (Math.abs(balance)).toString().replace('.', ',');
                        debtInquiryLink.parent().parent().find('input[type=text].payment_amount').val(balanceStr);
                    }
                    else {
                        debtInquiryLink.parent().find('.balance_type').text($('.imposs_to_get_balance_part1').text());
                        debtInquiryLink.parent().find('.balance_timestamp').text($('.imposs_to_get_balance_part2').text());
                    }

                    $(obj).remove();
                    $('.platezh_debt div.debt, .platezh_debt span.debt').show(500);
                }
            }
        }

        function onBalanceRequestError() {
            debtInquiryLink.parent().find('.balance_type').text($('.imposs_to_get_balance_part1').text());
            debtInquiryLink.parent().find('.balance_timestamp').text($('.imposs_to_get_balance_part2').text());
            $(obj).remove();
            $('.platezh_debt div.debt, .platezh_debt span.debt').show(500);
        }

        $.ajax({
            type: "POST",
            url: serviceUrl,
            contentType: "application/json; charset=utf-8",
            data: requestData,
            dataType: "json",
            success: function (msg) {
                onBalanceRequestSuccess(msg);
            },
            error: function () {
                onBalanceRequestError();
            }
        });
    }
};

var intervalId;

jQuery(document).ready(function (e) {

    $('.platezhi_central_inner_tabs_menu div').click(function () { // переключаем пункты меню "Платежи / История платежей" --
        $('.platezhi_central_inner_tabs_menu div').removeClass('active');
        $(this).addClass('active');
        $('.platezhi_central_inner_tabs > div').removeClass('active');
        $('.platezhi_central_inner_tabs #platezhi_central_inner_tab_' + this.id).addClass('active');
        if ('platezhi_central_inner_tab_' + this.id == 'platezhi_central_inner_tab_payments_history') {
            payments.main.hide_failed_payment_icon();
            //intervalId = setInterval(function () {
            //    payments.main.hide_failed_payment_icon();
            //}, 3000);
        } else {
            //clearInterval(intervalId);
        }
        main.contentHeight.resizeFrame();
    }); // переключаем пункты меню "Платежи / История платежей" --

    $('#platezhi_central_inner_tab_payments div.platezh:not(.active)').click(function () { // добавляем класс active при выборе строки платежа --
        $('#platezhi_central_inner_tab_payments .platezh').removeClass('active');
        $(this).addClass('active');

        /*if ($(this).hasClass('new_platezh')) {
			//$(this).removeClass('new_platezh');
			$(this).children('.debt').show();
		}*/

    }); // -- добавляем класс active при выборе строки платежа

    $('.platezh_name_outer ins').click(function () { // отрабатываем событие click-а на иконке карандаша в строке платежа --
        $($(this).parent()).children('div').click();
    }); // -- отрабатываем событие click-а на иконке карандаша в строке платежа

    $('.platezh_name').click(function () { // при клике на имени платежа заменяем его на поле ввода --
        //if ($("input").is("#platezh_name_input") == false) {
        //    var html = $(this).html();
        //    $(this).html('');
        //    var obj = $("<input></input>").attr({ name: 'platezh_name_input', id: 'platezh_name_input', value: html, maxlength: 225 });
        //    $(this).append(obj);
        //    $(obj).focus();
        //    $($(this).parent()).children('ins').hide();
        //    $('#platezh_name_input').blur(function () { // сохраняем новое имя платежа --
        //        $($($(this).parent()).parent()).children('ins').show();
        //        $(this).parent().html($(this).val());
        //    }); // -- сохраняем новое имя платежа
        //}
        if ($("input").is("#platezh_name_input") == false) {
            var trigger = $(this);
            var subscriptionId = trigger.attr('subscription_id');
            var oldName = trigger.html().trim();
            trigger.html('');
            var obj = $("<input></input>").attr({ name: 'platezh_name_input', id: 'platezh_name_input', value: oldName, maxlength: 225 });
            trigger.append(obj);
            $(obj).focus();
            $(trigger.parent()).children('ins').hide();

            function showSubscriptionName(name) {
                $(trigger.parent()).children('ins').show();
                trigger.html(name);
            }

            $('#platezh_name_input').on('keypress blur change', function (event) { // сохраняем новое имя платежа --
                if (event.type == 'blur' || event.type == 'change' || (event.type == 'keypress' && event.keyCode == 13) || $(this).val().length >= 100) {
                    var newName = $(this).val().trim();
                    if (newName != oldName) {
                        //if (/^[()[].+=:,;!?-'_\sа-яА-Яa-zA-Z0-9]{1,100}$/.test(newName)) {
                        if (newName.length > 0 && newName.length <= 100) {
                            $.ajax({
                                type: "POST",
                                url: "PrivateServices/TelebankBackendService.asmx/RenameSubscription",
                                contentType: "application/json; charset=utf-8",
                                data: '{ "id":"' + subscriptionId + '", "name":"' + newName + '"}',
                                dataType: "json",
                                success: function () {
                                    showSubscriptionName(newName);
                                },
                                error: function () {
                                    showSubscriptionName(oldName);
                                }
                            });
                        } else {
                            showSubscriptionName(oldName);
                        }
                    } else {
                        showSubscriptionName(oldName);
                    }
                }
                if (event.type == 'keypress' && event.keyCode == 13) {
                    return false;
                }
            }); // -- сохраняем новое имя платежа
        }
    }); // -- при клике на имени платежа заменяем его на поле ввода

    $('.platezh_sum_bloc input[type=text]').keyup(function () { // при вводе в строку суммы платежа показываем строку "узнать задолженность" --
        $('.active .platezh_debt div.debt').hide(500, function () {
            $('.active .platezh_debt ins').css('display', 'block');
        });

        $('.b-provider__amount .platezh_debt span.debt').hide(500, function () {
            $('.b-provider__amount .platezh_debt ins').css('display', 'inline');
        });
    }); // -- при вводе в строку суммы платежа показываем строку "узнать задолженность"

    $('#category_select_bloc_inner div').click(function () { // при выборе пункта select-а "Категория платежа" скрываем выбор вариантов --
        $('#category_select_bloc_inner div').removeClass('active');
        $(this).addClass('active');
        $('.category_select_bloc').hide();
        $('.category_select_text').html($(this).html());
        $('.select_bloc_close').remove();
    }); // -- при выборе пункта select-а "Категория платежа" скрываем выбор вариантов

    $('.category_select').click(function () { // при клике на "Категория платежа" показываем select --
        $('.category_select_bloc').show();
        $('.category_select_bloc').tinyscrollbar();
        $(document.body).append($('<div></div>').addClass('select_bloc_close').click(function () {
            $('.category_select_bloc').hide();
            $(this).remove();
        }));
    }); // -- при клике на "Категория платежа" показываем select

    $('.platezh .delete ins').click(function (e) { // событие удаления платежа --
        var del_obj = $(this);
        del_obj.addClass('active');
        $('.delete_confirm').show().css({ 'top': parseInt($(this).offset().top) - 100 + 'px', 'left': parseInt($(this).offset().left) - 337 + 'px' });
        $('.delete_confirm .image').html($($(this).parents('.platezh')).children('.image').html());
        $('.delete_confirm .title .name').html($($(this).parents('.platezh')).find('.platezh_name').html());
        $('.delete_confirm .title .info').html($($(this).parents('.platezh')).find('.platezh_info').html());
        $(document.body).append($('<div></div>').addClass('platezh_del_close').click(function () {
            $('.delete_confirm').hide();
            $('.platezh .delete ins').removeClass('active');
            $(this).remove();
        }));

        $('.delete_confirm button.light_grey').click(function (arg) {
            arg.preventDefault();
            $('.delete_confirm').hide();
            $('.platezh .delete ins').removeClass('active');
            $('.platezh_del_close').remove();
        });

        var subscriptionId = del_obj.attr('subscription_id');

        $('.delete_confirm button.blue').click(function (arg) {
            arg.preventDefault();
            $.ajax({
                type: "POST",
                url: "PrivateServices/TelebankBackendService.asmx/DeleteSubscription",
                contentType: "application/json; charset=utf-8",
                data: '{ "id":"' + subscriptionId + '"}',
                dataType: "json",
                success: function () {
                    del_obj.parents('.platezh').remove();
                }
            });
            $('.delete_confirm').hide();
            $('.platezh .delete ins').removeClass('active');
            $('.platezh_del_close').remove();
            arg.stopPropagation();
        });
    }); // -- событие удаления платежа

    $('#platezhi_central_inner_tab_payments_history div.payments_history:not(.active)').click(function () { // добавляем класс active при выборе строки платежа --
        $('#platezhi_central_inner_tab_payments_history .payments_history').removeClass('active');
        $(this).addClass('active');
    }); // -- добавляем класс active при выборе строки платежа

    // *********************** my mod ***********

    $('.platezhi_logo_tabs_inner div ins[payment_type=regular]').click(function () {
        var ins = $(this);
        if (ins.hasClass('logo_disable')) {
            return false;
        }
        var serviceId = ins.attr("service_id");
        document.location.href = 'index.aspx?action=payment-wizard&service_id=' + serviceId;
        return false;
    });

    $('.platezh button[payment_type=regular]').click(function () {
        var button = $(this);
        var subscriptionId = button.attr("subscription_id");
        var amount = 0;
        if (button.parent().parent().find("input[type=text]").size())
        amount = button.parent().parent().find("input[type=text]").val().trim();

        document.location.href = 'index.aspx?action=payment-wizard&subscription_id=' + subscriptionId + '&amount=' + amount;
        $('#platezhi_central_inner_tab_payments').mask('Обработка...');
        return false;
    });

    $('.platezhi_logo_tabs_inner div ins[payment_type=invoice]').click(function () {
        var ins = $(this);
        if (ins.hasClass('logo_disable')) {
            return false;
        }
        var serviceId = ins.attr("service_id");
        document.location.href = 'index.aspx?action=payment-invoice-wizard&service_id=' + serviceId;
        return false;
    });

    $('.platezh button[payment_type=invoice]').click(function () {
        var button = $(this);
        var subscriptionId = button.attr("subscription_id");
        var activeInvoiceId = button.attr("active_invoice_id");
        var serviceId = button.attr("service_id");

        document.location.href = 'index.aspx?action=payment-invoice-wizard&subscription_id=' + subscriptionId + '&active_invoice_id=' + activeInvoiceId + '&service_id=' + serviceId;
        $('#platezhi_central_inner_tab_payments').mask('Обработка...');
        return false;
    });

    function attachFailedPaymentMessagePopup() {
        $('.payments_history.false .payment_hist_pseudo').click(function (event) {
            event.preventDefault();
            $(this).closest('.payments_history').find('.window_block').show();
        });

        //close window
        $('.payments_history .window_block_close').click(function () {
            $(this).closest('.window_block').hide(1);
        });

    }

    function getPaymentHistory() {
        var request = {
            startDateValue: $('.calendar_select_date .cs_begin').text().trim(),
            endDateValue: $('.calendar_select_date .cs_finish').text().trim()
        };

        $.ajax({
            type: "POST",
            url: "api/payments/get-history",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(request),
            success: function (data) {
                if (data.length > 0) {
                    $('.payments_history').remove();
                    $('.payment_log_items').html(data);
                }
                attachFailedPaymentMessagePopup();  
                main.contentHeight.resizeFrame();
                $('.payment_log_items').unmask();
            },
            error: function () {
                $('.payment_log_items').unmask();
            }
        });
    }

    $('#period_show_bt').click(function (arg) {
        arg.preventDefault();
        $('.payment_log_items').mask('обработка');
        getPaymentHistory();
    });

    function getPaymentStatus() {
        var paymentList = new Array();
        $('.payments_history').each(function () {
            var historyItem = $(this);
            var status = historyItem.attr('payment_status');
            //|| status == "-202"
            if (status == "1" || status == "-200" || status == "-201") {
                paymentList.push(historyItem.attr('payment_id'));
            }
        });
        if (paymentList.length > 0) {
            $.ajax({
                type: "POST",
                url: "api/payments/get-status",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(paymentList),
                dataType: "json",
                success: function (data) {
                    //if (data.d != "") {
                    //    if (data.d == "logout") {
                    //        window.location.href = "Logout.aspx";
                    //    } else {
                    var obtainedPayments = data;

                    for (var i = 0; i < obtainedPayments.length; i++) {

                        var payment = obtainedPayments[i];

                        $('.payments_history').each(function () {

                            var historyItem = $(this);
                            var paymentId = historyItem.attr('payment_id');
                            var oldStatus = historyItem.attr('payment_status');
                            
                            if (paymentId == payment.Id && oldStatus !== payment.Status) {

                                historyItem.attr('payment_status', payment.Status);
                                historyItem.attr('class', 'payments_history');

                                var paymentStatusLabel = historyItem.find('.payment_status_text');

                                switch (payment.Status) {
                                    case 0:
                                        historyItem.addClass('true');
                                        historyItem.animate({ 'margin-left': '-10px' }, 80, function () {
                                            historyItem.animate({ 'margin-left': '10px' }, 80, function () {
                                                paymentStatusLabel.text($('.msg_payment_processed').text());
                                                var receiptLink = historyItem.find('#hlShowReceipt');
                                                receiptLink.prop("href", receiptLink.attr("href") + payment.AuthRef);
                                                historyItem.animate({ 'margin-left': '0px' });
                                            });
                                        });
                                        break;
                                    case -200:
                                        historyItem.addClass('during');
                                        paymentStatusLabel.text($('.msg_payment_being_processed').text());
                                        payments.main.show_failed_payment_icon();
                                        break;
                                    case -201:
                                        if (oldStatus != -1) {
                                            historyItem.addClass('false');
                                            paymentStatusLabel.text($('.msg_payment_canceled').text());
                                            var receiptLink = historyItem.find('#hlShowReceipt');
                                            receiptLink.prop("href", "");
                                            historyItem.find('.window_block_message_text').html($('<textarea/>').html(payment.FailureMessage).val());
                                            historyItem.find('.payment_hist_pseudo').click(function () {
                                                event.preventDefault();
                                                historyItem.find('.window_block').show();
                                            });
                                            payments.main.show_failed_payment_icon();
                                        }
                                        break;
                                    case -202:
                                    default:
                                        if (oldStatus != -1) {
                                            historyItem.addClass('false');
                                            historyItem.animate({ 'margin-left': '-10px' }, 80, function () {
                                                historyItem.animate({ 'margin-left': '10px' }, 80, function () {
                                                    historyItem.animate({ 'margin-left': '0px' });
                                                });
                                            });
                                            paymentStatusLabel.text($('.msg_payment_failed').text());
                                            var receiptLink = historyItem.find('#hlShowReceipt');
                                            receiptLink.prop("href", "");
                                            historyItem.find('.window_block_message_text').html($('<textarea/>').html(payment.FailureMessage).val());
                                            historyItem.find('.payment_hist_pseudo').click(function () {
                                                event.preventDefault();
                                                historyItem.find('.window_block').show();
                                            });
                                            payments.main.show_failed_payment_icon();
                                        }
                                        break;
                                }
                            };
                        });
                    }; //}

                    $('.payments_history.false .payment_hist_pseudo').click(function (event) {
                        event.preventDefault();
                        $(this).closest('.payments_history').find('.window_block').show();
                    });
                    //}
                },
                error: function () {
                }
            });
        };
    }

    setInterval(function () {
        getPaymentStatus();
    }, 10000);
    getPaymentStatus();

    payments.main.init();

    attachFailedPaymentMessagePopup();

    // ******************************************
});